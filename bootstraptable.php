<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title> Bootstrap Columns </title>

	<link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script type="text/javascript" src="js/smartarts.js"></script>

</head>

<body>

	<!-- Navigation Bar -->
	<nav class="navbar">

			<ul>

				<li> <a href="index.php">Home</a> </li>
				<li> <a href="autoloader.php">Autoloader</a> </li>
				<li> <a href="bootstraptable.php">Bootstrap Columns</a> </li>
		
			</ul>

	</nav>

		<!-- Landing Page -->
		<header>
			
		</header>

	


		        <div class="container">

            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2> Example of using Bootstrap Columns </h2>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-4 text-center">
						<h3> Column 1 </h3>
                        <h4> Example 1 </h4>
                        <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat  </p>
                </div>

                <div class="col-lg-4 text-center">
                    <h3> Column 2 </h3>
                    <h4> Example 2 </h4>
                    <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
                </div>

                <div class="col-lg-4 text-center">
                    <h3> Column 3 </h3>
                    <h4> Example 3 </h4>
                    <p> Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca. </p>
                </div>

            </div>
           
        </div>










	<div class="footerarea">
		<footer>

			<h3> Footer Area </h3>
			
		</footer>
	</div>	

</body>
</html> 