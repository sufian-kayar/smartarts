<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title> Home </title>

	<link href="css/style.css" rel="stylesheet">
	<script type="text/javascript" src="js/smartarts.js"></script>

</head>

<body>

	<!-- Navigation Bar -->
	<nav class="navbar">

			<ul>

				<li> <a href="index.php">Home</a> </li>
				<li> <a href="autoloader.php">Autoloader</a> </li>
				<li> <a href="bootstraptable.php">Bootstrap Columns</a> </li>
		
			</ul>

	</nav>

		<!-- Landing Page -->
		<header>
			
		</header>

	<div class="container">

		<!-- Main Content -->
		<section id="first">

        	<h1 class="heading">Quick and Simple web page </h1>

        	<div class="text" id="demo"> A list of tasks which I have completed will appear in 5 seconds. </div>

		</section>	

	</div>	

	<div class="footerarea">
		<footer>

			<h3> Footer Area </h3>
			
		</footer>
	</div>	

</body>
</html> 