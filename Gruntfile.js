module.exports = function(grunt) {

  // Project configuration.

  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

    cssmin: {
      my_target: {
          files: [{
          expand: true,
          cwd: 'css/',
          src: ['*.css', '!*.min.css'],
          dest: 'css/',
          ext: '.min.css'
          }]
      }
    }
      /*
          uglify: {
          my_target: {
              files: [{
                    src: 'js/*.js',
                    dest: 'js/',
                    expand: true,
                    flatten: true,
                    ext: '.min.js'
              }]
          }
      }*/
  });
  

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);

};