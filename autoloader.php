<?php 
session_start();

function __autoload($class_name) {
  require_once 'classes/'.$class_name . '.php';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title> Example Work </title>

	<link href="css/style.css" rel="stylesheet">
	<script type="text/javascript" src="js/smartarts.js"></script>

</head>

<body>

	<!-- Navigation Bar -->
	<nav class="navbar">

			<ul>

				<li> <a href="index.php">Home</a> </li>
				<li> <a href="autoloader.php">Autoloader</a> </li>
				<li> <a href="bootstraptable.php">Bootstrap Table</a> </li>
		
			</ul>

	</nav>

		<!-- Landing Page -->
		<header>
			
		</header>

	<div class="container">

		<!-- Main Content -->
		<section id="first">

        	<h1 class="heading">Quick and Simple web page </h1>


		<h2> Item Class Test </h2>

		<div class="text"> A random ball class has been created to demonstrate the use of the AUTOLOADER. </div>
		
		<?php
			print "<h4> List of Footballs Available </h4>";

			$ball1 = new Ball("Nike", "5", "grass", "professional");
			print "Brand: " . $ball1->getBrand();
			print "<br>";
			print "Size: " . $ball1->getSize();
			print "<br>";
			print "Surface: " . $ball1->getSurface();
			print "<br>";
			print "Level: " . $ball1->getLevel();
			print "<br> <br>";




			$ball2 = new Ball("Addidas", "5", "artificial grass", "intermediate");
			print "Brand: " . $ball2->getBrand();
			print "<br>";
			print "Size: " . $ball2->getSize();
			print "<br>";
			print "Surface: " . $ball2->getSurface();
			print "<br>";
			print "Level: " . $ball2->getLevel();
			print "<br> <br>";




			$ball3 = new Ball("Puma", "4", "artificial grass", "intermediate");
			print "Brand: " . $ball3->getBrand();
			print "<br>";
			print "Size: " . $ball3->getSize();
			print "<br>";
			print "Surface: " . $ball3->getSurface();
			print "<br>";
			print "Level: " . $ball3->getLevel();
			print "<br> <br>";

		?>     	
		
		</section>	

	</div>	

	<div class="footerarea">
		<footer>

			<h3> Footer Area </h3>
			
		</footer>
	</div>	

</body>
</html> 